﻿#include <vector>
#include <iostream>
#include <chrono>

void Bubblesort(int n, std::vector <int>& mas)
{
    for (int i = 1; i < n; i++)
    {
        for (int j = i - 1; j >= 0; j--)
        {
            if (mas[j] > mas[j + 1])
            {
                std::swap(mas[j], mas[j + 1]);
            }
        }
    }
}


void Quicksort(int a, int b, std::vector <int>& mas)
{
    if (a >= b) return;
    int m = rand() % (b - a + 1)  +a;
    int k = mas[m];
    int l = a - 1;
    int r = b + 1;
    while (1)
    {
        do l = l + 1; while (mas[l] < k);
        do r = r - 1; while (mas[r] > k);
        if (l >= r) 
        {
            break;
        }
        std::swap(mas[l], mas[r]);
    }
    r = l;
    l = l - 1;
    Quicksort(a, l, mas);
    Quicksort(r, b, mas);
}


class Timer
{
private:
    using clock_t = std::chrono::high_resolution_clock;
    using second_t = std::chrono::duration<double, std::ratio<1> >;

    std::chrono::time_point<clock_t> m_beg;

public:
    Timer() : m_beg(clock_t::now())
    {
    }

    void reset()
    {
        m_beg = clock_t::now();
    }

    double elapsed() const
    {
        return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
    }
};

int main()
{
    int n;
    std::cin >> n;
    std::vector <int> mas(n);

    for (int i = 0; i < n; i++)
    {
        mas[i] = rand();
        //std::cout << mas[i] << std::endl;
    }
    
    Timer t;
    Quicksort(0, n - 1, mas);
    std::cout << "Time elapsed: " << t.elapsed() << '\n';
    for (int i = 0; i < n; i++)
    {
        //std::cout << mas[i] << std::endl;
    }
    
    Timer b;
    Bubblesort (n,mas);
    std::cout << "Time elapsed: " << b.elapsed() << '\n';
    for (int i = 0; i < n; i++)
    {
       // std::cout << mas[i] << std::endl;
    }
}


